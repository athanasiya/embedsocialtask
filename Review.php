<?php

class Review 
{
    use JsonDB;

    private $id;
    private $reviewText;
    private $rating;
    private $reviewCreatedOnDate;

    public function __construct($data)
    {
        $this->setId($data['id'])->setReviewText($data['reviewText'])->setRating($data['rating'])->setReviewDate($data['reviewCreatedOnDate']);
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setReviewText($reviewText)
    {
        $this->reviewText = $reviewText;
        return $this;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    public function setReviewDate($reviewCreatedOnDate)
    {
        $this->reviewCreatedOnDate = $reviewCreatedOnDate;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getReviewText()
    {
        return $this->reviewText;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function getReviewDate()
    {
        return $this->reviewCreatedOnDate;
    }

    public function print() {
        echo "<tbody>
                <tr>
                    <td>{$this->getId()}</td>
                    <td>{$this->getReviewText()}</td>
                    <td>{$this->getRating()}</td>
                    <td>{$this->getReviewDate()}</td>
                </tr>
            </tbody>";
    }

    public static function getFilename()
    {
        return 'reviews.json';
    }
}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="w-50 mx-auto">
            <form action="filterReview.php" method="POST">
                <fieldset>
                    <legend>Filter Reviews</legend>
                    <div class="mb-3">
                        <label for="orderByRating" class="form-label">Order by rating:</label>
                        <select id="orderByRating" name="orderByRating" class="form-select">
                            <option value="highest">Highest First</option>
                            <option value="lowest">Lowest First</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="minimumRating" class="form-label">Minimum rating:</label>
                        <select id="minimumRating" name="minimumRating" class="form-select">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="orderByDate" class="form-label">Order by date:</label>
                        <select id="orderByDate" name="orderByDate" class="form-select">
                            <option value="oldest">Oldest First</option>
                            <option value="newest">Newest First</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="prioritizeByText" class="form-label">Prioritize by text:</label>
                        <select id="prioritizeByText" name="prioritizeByText" class="form-select">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">FIlter</button>
                </fieldset>
            </form>
        </div>
    </body>
</html>
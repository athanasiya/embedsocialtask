<?php

trait JsonDB
{
    public static function filter($orderByRating, $minimumRating, $orderByDate, $prioritizeByText)
    {
        $data = trim(file_get_contents(self::getFilename()));
        $data = json_decode($data, true);

        if($orderByDate == "oldest") {
            usort($data, function ($a, $b) {
                return (strtotime($a["reviewCreatedOnDate"]) - strtotime($b["reviewCreatedOnDate"]));
            });
        } else if($orderByDate == "newest") {
            usort($data, function ($a, $b) {
                return (strtotime($b["reviewCreatedOnDate"]) - strtotime($a["reviewCreatedOnDate"]));
            });
        }
    
        if($orderByRating == "lowest") {
            usort($data, function ($a, $b) {
                return strcmp($a["rating"], $b["rating"]);
            });
        } else if($orderByRating == "highest") {
            usort($data, function ($a, $b) {
                return strcmp($b["rating"], $a["rating"]);
            });
        }

        if($prioritizeByText == "yes") {
            $count = 0;
            foreach($data as $review) {
                $count++;
                if($review['reviewText'] == "") {
                    unset($data[$count-1]);
                    array_push($data, $review);
                }
            }
        }
    
        $reviews = [];

        foreach ($data as $review) {
            array_push($reviews, new self($review));
        }

        return $reviews;
    }

    abstract public static function getFilename();
}